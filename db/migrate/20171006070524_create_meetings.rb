class CreateMeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :meetings do |t|
      t.datetime :meeting_date
      t.text :description
      t.string :type

      t.timestamps
    end
  end
end
