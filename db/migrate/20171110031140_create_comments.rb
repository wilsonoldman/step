class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :user, foreign_key: true
      t.references :meeting, foreign_key: true
      t.text :description, null: false
      t.timestamps
    end
    add_index :comments, %i[user_id meeting_id]
  end
end
