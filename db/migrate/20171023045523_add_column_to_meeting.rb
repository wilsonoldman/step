class AddColumnToMeeting < ActiveRecord::Migration[5.1]
  def change
    change_table :meetings do |t|
      t.datetime :end_at
    end
  end
end
