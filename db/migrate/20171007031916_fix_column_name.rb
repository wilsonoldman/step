class FixColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :meetings, :meeting_date, :start_date
    rename_column :meetings, :type, :category
  end
end
