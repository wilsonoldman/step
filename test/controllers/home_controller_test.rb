require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get index_url
    assert_response :success
  end

  test 'should get about' do
    get about_url
    assert_response :success
  end

  test 'should get external_links' do
    get external_links_url
    assert_response :success
  end

  test 'should get history' do
    get history_url
    assert_response :success
  end
  test 'should get introduction' do
    get introduction_url
    assert_response :success
  end

  test 'should get joinu' do
    get joinus_url
    assert_response :success
  end

  test 'should get schedule' do
    get schedule_url
    assert_response :success
  end

end
