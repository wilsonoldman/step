# == Schema Information
#
# Table name: meetings
#
#  id          :integer          not null, primary key
#  start_date  :datetime
#  description :text
#  category    :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  end_at      :datetime
#

require 'test_helper'

class MeetingTest < ActiveSupport::TestCase
  def setup
    @meeting = meetings(:now)
  end

  test 'meeting should be valid' do
    assert @meeting.valid?
  end

  test 'start_date should be present' do
    @meeting.start_date = ' '
    assert_not @meeting.valid?
  end

  test 'end_at should be present' do
    @meeting.end_at = nil
    assert_not @meeting.valid?
  end

  test 'description length should be less than 1024' do
    @meeting.description = 'a' * 1025
    assert_not @meeting.valid?
  end

end
