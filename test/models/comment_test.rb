# == Schema Information
#
# Table name: comments
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  meeting_id  :integer
#  description :text             not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  def setup
    @comment = comments(:one)
    @admin = users(:admin)
    @meeting = meetings(:now)
  end

  test 'fixture should be valid' do
    assert @comment.valid?
  end

  test 'description should be present' do
    @comment.description = ''
    assert_not @comment.valid?
  end

  test 'user_id should be associated with users.id' do
    @comment.user_id = 000
    assert_not @comment.valid?

    @comment.user_id = @admin.id
    assert @comment.valid?
  end

  test 'meeting_id should be associated with meetings.id' do
    @comment.meeting_id = 000
    assert_not @comment.valid?

    @comment.meeting_id = @meeting.id
    assert @comment.valid?
  end
end
