require 'test_helper'

class MeetingsEditTestTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  def setup
    @admin = users(:admin)
    @meeting = meetings(:now)
  end

  test 'successful edit' do
    sign_in(@admin)
    get edit_meeting_path(@meeting)
    s_date = Time.zone.now + 1.day
    e_at = Time.zone.now + 1.day + 2.hours
    patch meeting_path(@meeting),
          params: {
            meeting: {
              start_date: s_date,
              end_at: e_at,
              category: 'event',
              description: 'foobar'
            }
          }
    assert_redirected_to meetings_path
    assert_not flash.empty?
    @meeting.reload
    assert_equal @meeting.start_date.to_s, s_date.to_s
    assert_equal @meeting.end_at.to_s, e_at.to_s
  end

end
