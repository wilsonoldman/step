# encoding: utf-8
require 'test_helper'

class MeetingCreateTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @admin_user = users(:admin)
    @general_user = users(:nonadmin)
    @meeting = meetings(:now)
  end

  test 'admin can create a meeting' do
    # general user
    get root_url
    assert_no_difference 'Meeting.count' do
      post meetings_path, params: {
        meeting: {
          start_date: Time.zone.now,
          end_at: Time.zone.now + 2.hours,
          category: 'event',
          description: 'general'
        }
      }
    end
    assert_not flash.empty?

    # admin user
    sign_in(@admin_user)
    get root_url
    assert_difference 'Meeting.count', 1 do
      post meetings_path, params: {
        meeting: {
          start_date: Time.zone.now,
          end_at: Time.zone.now + 2.hour,
          category: 'イベント',
          description: 'foobar'
        }
      }
    end
  end
end
