# encoding=utf-8
require 'test_helper'

class AdminMeetingsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @admin_user = users(:admin)
    @general_user = users(:nonadmin)
    @meeting = meetings(:now)
  end

  test 'anonymous can not destroy a meeting' do
    get meetings_path
    assert_no_difference 'Meeting.count' do
      delete meeting_path(@meeting)
    end
    assert_redirected_to new_user_session_url
  end

  test 'only admin can destroy a meeting' do
    # general user
    sign_in(@general_user)
    get meetings_path
    assert_select 'input.btn.btn-danger', count: 0, text: 'delete'
    assert_no_difference 'Meeting.count' do
      delete meeting_path(@meeting)
    end
    assert_redirected_to root_url
    assert_not flash.empty?

    # admin user
    sign_in(@admin_user)
    get meetings_path
    assert_select 'input.btn.btn-danger'
    assert_difference 'Meeting.count', -1 do
      delete meeting_path(@meeting)
    end
    assert_redirected_to meetings_path
    assert_not flash.empty?
  end

  test 'only admin can edit meetings' do
    # general_user
    sign_in(@general_user)
    get edit_meeting_path(@meeting)
    assert_redirected_to root_url
    assert_not flash.empty?

    # admin user
    sign_in(@admin_user)
    get edit_meeting_path(@meeting)

  end
end
