require 'test_helper'

class CommentCreateTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @admin = users(:admin)
    @nonadmin = users(:nonadmin)
    @meeting = meetings(:now)
    @comment = Comment.new
  end

  test 'anonymous can not post comment' do
    get meetings_path
    assert_no_difference 'Comment.count' do
      post comments_path(@comment), params: {
        comment: {
          description: 'foobar',
          user_id: @nonadmin.id,
          meeting_id: @meeting.id
        }
      }
    end
    assert_redirected_to new_user_session_url
    assert_not flash.empty?
  end

  test 'successful comment post' do
    sign_in(@admin)
    get meetings_path
    assert_difference 'Comment.count', 1 do
      post comments_path(@comment), params: {
        comment: {
          description: 'foobar',
          user_id: @admin.id,
          meeting_id: @meeting.id
        }
      }
    end
  end

  test 'unsuccessful comment post' do
    # comment user_id must be equal to current user_id
    sign_in(@admin)
    get meetings_path
    assert_no_difference 'Comment.count' do
      post comments_path(@comment), params: {
        comment: {
          description: 'foobar',
          user_id: @nonadmin.id,
          meeting_id: @meeting.id
        }
      }
    end
  end
end
