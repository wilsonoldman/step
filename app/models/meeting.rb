# encoding: utf-8

# == Schema Information
#
# Table name: meetings
#
#  id          :integer          not null, primary key
#  start_date  :datetime
#  description :text
#  category    :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  end_at      :datetime
#

class Meeting < ApplicationRecord
  has_many :comments, -> { order(:created_at) }, dependent: :destroy
  has_many :users, through: :comments

  validates :start_date, presence: true
  validates :end_at, presence: true
  validates :category, presence: true
  validates :description, allow_nil: true, length: { maximum: 1024 }
  validate :start_date_must_before_end_at

  before_validation :format_end_at

  # scopes
  scope :future_meetings, -> { where('start_date >= ?', Time.zone.now.midnight) }

  private
  # custom validations
  def start_date_must_before_end_at
    if !start_date.is_a?(Time) || !end_at.is_a?(Time) || end_at <= start_date
      errors.add(:end_at, '時刻を正しく設定してください')
    end
  end

  # callbacks
  def format_end_at
    return if !start_date.is_a?(Time) || !end_at.is_a?(Time)
    end_at_time = end_at - end_at.midnight
    self.end_at = start_date.midnight + end_at_time
    self.end_at += 1.day if start_date >= end_at
  end
end
