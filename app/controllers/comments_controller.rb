# encoding: utf-8

class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = Comment.new(comment_params)
    if comment_user_equal_to_current? && @comment.save
      flash[:success] = 'コメントを投稿しました。'
      redirect_to meetings_path
    else
      flash.now[:danger] = @comment.errors.full_messages
      session[:comment_description] = @comment.description
      redirect_to meetings_path
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:description, :user_id, :meeting_id)
  end

  def comment_user_equal_to_current?
    current_user.id == comment_params[:user_id].to_i
  end
end
