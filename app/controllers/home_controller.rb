class HomeController < ApplicationController
  def index
    @future_meetings = Meeting.future_meetings.order(:start_date).take(2)
    @next_meeting = @future_meetings[0]
    @after_next_meeting = @future_meetings[1] if @future_meetings[1]
    @meeting = Meeting.new
  end

  def introduction; end

  def about; end

  def joinus; end

  def staff; end

  def history; end

  def voice; end

  def rules; end

  def external_links; end

end
