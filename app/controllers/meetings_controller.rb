# encoding: utf-8

class MeetingsController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :user_admin, except: [:index]

  def index
    @comment = Comment.new
    @meetings = Meeting.order(start_date: :desc).includes(:comments).includes(:users).page params[:page]
  end

  def create
    @meeting = Meeting.new(meeting_params)
    cookies[:meeting_category] = @meeting.category
    cookies[:meeting_description] = @meeting.description
    if @meeting.save
      redirect_to root_path, flash: { success: '予定を作成しました。' }
    else
      flash.now[:danger] = @meeting.errors.full_messages
      redirect_to root_url, flash: { danger: @meeting.errors.full_messages }
    end
  end

  def edit
    @meeting = Meeting.find(params[:id])
  end

  def update
    @meeting = Meeting.find(params[:id])
    if @meeting && @meeting.update_attributes(meeting_params)
      flash[:success] = 'スケジュールを更新しました。'
      redirect_to meetings_path
    else
      flash.now[:danger] = @meeting.errors.full_messages
      render :edit
    end
  end

  def destroy
    @meeting = Meeting.find(params[:id])
    if @meeting.destroy
      redirect_to meetings_path, flash: { success: '予定を削除しました。' }
    else
      flash[:danger] = @meeting.errors.full_messages
      redirect_to root_url
    end
  end

  private

  def user_admin
    unless current_user.try(:admin?)
      redirect_back(fallback_location: root_url)
      flash[:danger] = 'you are not authenticated!'
    end
  end

  def meeting_params
    params.require(:meeting).permit(:start_date, :end_at, :description, :category)
  end
end
