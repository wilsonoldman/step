Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  resources :meetings, except: %i[new show]
  resources :comments, only: %i[create destroy]
  root 'home#index'
  get '/index' => 'home#index', as: :index
  get '/introduction' => 'home#introduction', as: :introduction
  get '/about' => 'home#about', as: :about
  get '/joinus' => 'home#joinus', as: :joinus
  get '/staff' => 'home#staff', as: :staff
  get '/schedule' => 'meetings#index', as: :schedule
  get '/history' => 'home#history', as: :history
  get '/voice' => 'home#voice', as: :voice
  get '/rules' => 'home#rules', as: :rules
  get '/links' => 'home#external_links', as: :external_links
end
